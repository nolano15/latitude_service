/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package tools;

import java.io.IOException;

/**
 * Struct of useful filters
 */
public class Filters {
	// specified bounds
	public static final double LONGITUDE_MAX = 180.0,
            					LATITUDE_MAX = 90.0;
	// 2^x power bounds
    public static final int UNSIGNED_INT_MAX_POWER = 32,
    						PORT_MAX_POWER = 16;
    
    /**
     * Filters an ID through boundary tests
     * 
     * @param id ID being filtered
     * @param x binary power bound
     * 
     * @returns filtered ID
     * 
     * @throws IllegalArgumentException
     *      if ID is out of range
     */
    public static long filterId(long id, int x) throws IllegalArgumentException {
    	// testing if there needs to be an error message
        String message =
                id >= Math.pow(2, x) ?
                        "too large ID" : id < 0 ?
                                "Negative ID" : null;

        if(message != null) {
            throw new IllegalArgumentException(message);
        }
        return id;
    }
    
    /**
     * Filters a token through validation tests
     * 
     * @param token token to be filtered
     * 
     * @returns filtered token
     * 
     * @throws IllegalArgumentException
     *      if token is null or non-ASCII
     */
    public static String filterASCII(String token) throws IllegalArgumentException {
        if(token == null) {
            throw new IllegalArgumentException("null token");
        }
        
        // RegEx class from the first ASCII value, a space, to the last, a tilde
        // better than looping through each char
        if(!token.matches("[ -~]*")) {
            throw new IllegalArgumentException("non-ASCII token");
        }
        
        return token;
    }
    
    /**
     * Filters a Double long/latitude through boundary tests
     * 
     * @param longlat double value to filter
     * @param max long/lat bound
     * 
     * @return filtered long/latitude
     * 
     * @throws IllegalArgumentException
     * 		if out of range
     */
    public static double filterLongLat(String longlat, double max) throws IllegalArgumentException {
    	String message;
        
        try {
        	// testing if there needs to be an error message
            message = 
            		longlat.isEmpty() ?
                            "empty" : !longlat.matches("^-?[0-9]+\\.[0-9]+$") ?
                                    "Poorly formatted double" : Double.parseDouble(longlat) > max ?
                                            "too large" : Double.parseDouble(longlat) < max*-1 ?
                                                    "too negative" : null;
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("null", e);
        }
        
        if(message != null) {
            throw new IllegalArgumentException(message);
        }
        return Double.parseDouble(longlat);
    }
    
    /**
     * Filters token to see if I/O is wrong
     * 
     * @param token token to filter
     * 
     * @throws IOException
     *      if I/O problem is found
     */
    public static String filterIo(String token) throws IOException {
        // if token contains one of these, then throw an exception
        if(token.contains("\r") || token.contains("\n")) {
            throw new IOException("bad EOS");
        }
        return token;
    }
}
