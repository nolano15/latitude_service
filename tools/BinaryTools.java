/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package tools;

import java.nio.ByteBuffer;

/**
 * Struct of binary methods
 */
public class BinaryTools {
	
	/**
	 * Formats binary strings into standard 4 bit words
	 * 
	 * @param b byte to convert
	 * @param space space to format
	 * 
	 * @return formatted binary string
	 */
	public static String formatBits(int b, int space) {
		// add whitespace to enforce spacing, then replace with zeroes
		return String.format("%" + space + "s", Integer.toBinaryString(b)).replace(' ', '0');
	}
	
	/**
	 * Helper function to read length bytes from ByteBuffer
	 * 
	 * @param bb ByteBuffer to deserialize
	 * @param length num of bytes
	 * 
	 * @return array of read bytes
	 */
	public static byte[] getBuffBytes(ByteBuffer bb, int length) {
		// init buffer
		var buf = new byte[length];
		// read bytes
		bb.get(buf);
		
		return buf;
	}
	
	/**
	 * Reverses an array of bytes
	 * 
	 * @param b byte array to be reversed
	 * 
	 * @return reversed byte array
	 */
	public static final byte[] reverse(byte[] b) {
		// must only swap half the bytes
		for(int i = 0; i < b.length / 2; i++) {
			// standard swap algorithm
		    byte temp = b[i];
		    b[i] = b[b.length - i - 1];
		    b[b.length - i - 1] = temp;
		}
		return b;
	}
}
