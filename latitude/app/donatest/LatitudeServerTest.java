package latitude.app.donatest;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.*;

public class LatitudeServerTest {

    private static final String SERVER = "localhost";
    private static final int PORT = 5000;
    private static final String ENC = "ASCII";
    private static final int SLOWDELAYMS = 100;

    private Socket clientSocket;

    @BeforeEach
    protected void before() throws UnknownHostException, IOException {
        clientSocket = new Socket(SERVER, PORT);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        int v = clientSocket.getInputStream().read();
                        if (v == -1) {
                            System.out.println();
                            return;
                        }
                        System.out.print((char) v);
                    }
                } catch (IOException e) {
                }
            }
        }).start();
    }

    @AfterEach
    protected void after() throws IOException {
        clientSocket.close();
    }

    @BeforeAll
    protected static void announcement() {
    }

    @Test
    protected void TestBasic() throws Exception {
        printTest("Test Basic");

        printExpected("LATITUDEv1 345 RESPONSE 9 Class Map0");
        sendSlowly(clientSocket, "LATITUDEv1 345 ALL \r\n");
        

        printExpected("LATITUDEv1 345 RESPONSE 9 Class Map1 56 -97.12 31.55 20 Bob Smith: FortWorth9 Down town");
        sendSlowly(clientSocket, "LATITUDEv1 345 NEW 56 -97.12 31.55 9 FortWorth9 Down town\r\n");
        
        printExpected("LATITUDEv1 345 RESPONSE 9 Class Map1 56 -97.3 32.72 21 Bob Smith: Fort Worth8 Downtown");
        sendSlowly(clientSocket, "LATITUDEv1 345 NEW 56 -97.3 32.72 10 Fort Worth8 Downtown\r\n");

        printExpected("LATITUDEv1 345 RESPONSE 9 Class Map1 56 -97.3 32.72 21 Bob Smith: Fort Worth8 Downtown");
        sendSlowly(clientSocket, "LATITUDEv1 345 ALL \r\n");
    }
    
    @Test
    protected void TestProtocol() throws Exception {
        printTest("Test Protocol");

        printExpected("LATITUDEv1 345 ERROR 19 No such user ID: 57");
        sendSlowly(clientSocket, "LATITUDEv1 345 NEW 57 1.0 2.0 1 A1 B\r\n");
        
        printExpected("LATITUDEv1 348 ERROR 16 No such map: 348");
        sendSlowly(clientSocket, "LATITUDEv1 348 NEW 56 1.0 2.0 1 A1 B\r\n");
    }

    @Test
    protected void TestUnexpected() throws Exception {
        printTest("Test Unexpected");

        printExpected("LATITUDEv1 345 ERROR 30 Unexpected message type: ERROR");
        sendSlowly(clientSocket, "LATITUDEv1 345 ERROR 3 Boo\r\n");
    }
    
    private synchronized static void printTest(String testName) {
        System.err.println("***************************");
        System.err.println(testName);
        System.err.println("***************************");
    }

    private synchronized static void sendSlowly(Socket clientSocket, String msg) throws Exception {
        for (byte b : msg.getBytes(ENC)) {
            clientSocket.getOutputStream().write(b);
            TimeUnit.MILLISECONDS.sleep(SLOWDELAYMS);
        }
    }

    @SuppressWarnings("unused")
    private synchronized static void send(Socket clientSocket, String msg) throws Exception {
        clientSocket.getOutputStream().write(msg.getBytes(ENC));
        TimeUnit.MILLISECONDS.sleep(10);
    }

    private synchronized void printExpected(String msg) throws Exception {
        System.err.println(msg);
    }
}