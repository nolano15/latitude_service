/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 2
* Class: CS 4321
*
************************************************/

package latitude.app;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import latitude.serialization.*;

/**
 * TCP client that sends and receives LatitudeMessages
 */
public class LatitudeClient {
    
    /**
     * Defines Client interface with the Latitude service
     * 
     * @param args server and port #
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.print("Parameters: <Server> <Port>");
            System.exit(0); // terminate
        }
        String server = args[0]; // Server name or IP address
        int servPort = Integer.parseInt(args[1]);
        
        try(
                var socket = new Socket(server, servPort);
                var userIn = new Scanner(new BufferedReader(new InputStreamReader(System.in))) // wrap system.in
        ) {
            String yn; // will save yes/no value on continue prompt of do-while
            
            do {
            	// LatitudeMessage received from server
                LatitudeMessage received = null;
                try {
                    // immediately encode the user-specified, decoded LatitudeMessage
                    LatitudeMessage.decodeUser(userIn, new PrintStream(System.out))
                        .encode(new MessageOutput( socket.getOutputStream() ));
                    received = LatitudeMessage.decode(new MessageInput( socket.getInputStream() ));
                } catch (ValidationException e) { // handle invalid messages
                    System.err.print("Invalid message: " + e.getMessage());
                    System.exit(0); // terminate
                }
                
                // Handle unexpected messages
                if(!LatitudeLocationResponse.OPERATION.equals( received.getOperation() ) &&
                        !LatitudeError.OPERATION.equals( received.getOperation() )) {
                    System.err.println("Unexpected message: " + received);
                }
                else {
                    System.out.println("" + received);
                }
                
                // continue
                Boolean flag;
                do {
                    System.out.print("Continue (y/n) > ");
                    if(flag = !(yn = userIn.next()).matches("([yn])")) { // regex class, must match y or n
                        System.err.println("Invalid user input: Choose (y/n)");
                    }
                } while(flag); // loop until (y/n) is entered
                
            } while( "y".equals(yn) );
        } catch (IOException e) { // handle server comm probs
            System.err.print("Unable to communicate: " + e.getMessage());
            System.exit(0); // terminate
        }
    }
}