/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 3
* Class: CS 4321
*
************************************************/

package latitude.app;

import java.net.*;  // for Socket, ServerSocket, and InetAddress
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.*;   // for IOException and Input/OutputStream
import mapservice.*;
import mapservice.Location.Color;
import notifi.app.NoTiFiServer;
import latitude.serialization.*;

/**
 * Server that sends and receives LatitudeMessages, and updates a map
 */
public class LatitudeServer implements Runnable {
	/**
     * File containing JS-encoded array of locations
     */
    private static final String LOCATIONFILE = "markers.js",
                                MAPNAME = "Class Map", // default map name
                                LOGFILE = "connections.log";
    
    // default color
    private static final Color COLOR  = Location.Color.BLUE;
    // specified mapID
    private static final long MAPID = 345;
    private static final int TIMEOUT = 5000;   // Resend timeout (milliseconds)
    
    // Server logger
    protected Logger LOGGER;
    protected MemoryMapManager mgr;
    
    // data structures that will map userIDs to usernames and vice versa
    protected static Map<Long, String> userId2Name;
    protected static Map<String, Long> userName2Id;

    // Socket connect to client
	private Socket clntSock;
	
	/**
	 * Default constructor will format LOGGER
	 * 
	 * @param mgr MemoryMapManager to update
	 */
	public LatitudeServer(MemoryMapManager mgr, Logger LOGGER) {
		this.mgr = mgr;
		this.LOGGER = LOGGER;
		
		// link LOGGER to logfile
        FileHandler fh;
        try {
            fh = new FileHandler(LOGFILE);
            LOGGER.addHandler(fh);
            // human readable log
            fh.setFormatter(new SimpleFormatter());
        } catch (IOException e) {
            // failed to open logfile
            System.err.println(e.getMessage());
        }
        
        
	}
    
	// init client socket while multithreading
	/**
	 * Constructs TCP LatitudeServer
	 * 
	 * @param mgr MemoryMapManager to update
	 * @param clntSock client's port to connect to
	 */
    public LatitudeServer(MemoryMapManager mgr, Socket clntSock) {
    	// init LOGGER
    	this(mgr, Logger.getLogger( LatitudeServer.class.getName() ));
		this.clntSock = clntSock;
	}

	/**
     * Defines Latitude Server
     * 
     * @param args port, thread count, and password filename
     * 
     * @throws SocketException
     *      if NoTiFiServer failed to start
     */
    public static void main(String[] args) throws SocketException {
        if (args.length != 3) {  // Test for correct # of args
            System.err.print("Parameter(s): <Port> <Thread Count> <Password Filename>");
            System.exit(0); // terminate
        }
        
        // init args
        int servPort = Integer.parseInt(args[0]),
            nThreads = Integer.parseInt(args[1]);
        
        // load userData for instant access
        load(new File(args[2]));

        // Create map manager in memory
        var mgr = new MemoryMapManager();
        // Delete existing locations
        mgr.getLocations()
        	.parallelStream()
        	.forEach(l -> mgr.deleteLocation( l.getName() ));
        // Register listener to update MapBox location file
        mgr.register(new MapBoxObserver(LOCATIONFILE, mgr));
        
        // run NoTiFiServer
        new Thread(new NoTiFiServer(mgr, servPort)).start();
        
        // init thread pool
        Executor service = Executors.newFixedThreadPool(nThreads);
      
        // Create a server socket to accept client connection requests
        try(var servSock = new ServerSocket(servPort)){
            
            // enable SO_REUSEADDR
            servSock.setReuseAddress(true);
            
            while (true) { // Run forever, accepting and servicing connections
        		// Get client connection
            	Socket clntSock = servSock.accept();
                clntSock.setSoTimeout(TIMEOUT); // Maximum receive blocking time (milliseconds)
                
            	// spawn new thread
            	service.execute(new LatitudeServer(mgr, clntSock));
              }
        } catch(Exception e) {
            System.err.print( e.getMessage() );
        }
        System.exit(0); // kills all threads
    }
    
    /**
     * Run LatitudeClient interaction
     */
    @Override
	public void run() {
		System.out.println("Handling client " + clntSock.getRemoteSocketAddress()
        	+ " with thread id " + String.valueOf( Thread.currentThread().getId() ));
    
		try {
		    while(true) { // Run forever, until EOS
				// LatitudeMessage received from client
		        LatitudeMessage received = LatitudeMessage.decode(new MessageInput( clntSock.getInputStream() )),
		                        toSend = errFilters(received); // error-checked LatitudeMessage toSend to client
		        
		        // if no errors
		        if(toSend == null) {
		            if( LatitudeNewLocation.OPERATION.equals( received.getOperation() ) ) {
		                // extract NEW LocationRecord
		                var rcvdLocRec = ((LatitudeNewLocation)received).getLocationRecord();
		                
		                // load username from password file
		                String name = userId2Name.get( rcvdLocRec.getUserId() ) + ": " + rcvdLocRec.getLocationName();
		                
		                // update map
		                mgr.deleteLocation(name);
		                // client's new Location
		                var loc = new Location(name, rcvdLocRec.getLongitude(), rcvdLocRec.getLatitude(),
		                        rcvdLocRec.getLocationDescription(), COLOR);
		                // log update
		                LOGGER.info("New Location: " + loc);
		                mgr.addLocation(loc);
		            }
		            // send a RESPONSE no matter what
		            toSend = new LatitudeLocationResponse(received.getMapId(), MAPNAME);
		            
		            // add map locations to RESPONSE
		            for(Location loc : mgr.getLocations()) {
		                ((LatitudeLocationResponse)toSend).addLocationRecord( convertLoc(loc) );
		            }
		        }
		        toSend.encode(new MessageOutput( clntSock.getOutputStream() ));
		    }
		} catch(EOFException e) {
			// EOF means client finished
		} catch(Exception ex) {
        	try {
				new LatitudeError(0L, "Unable to parse message")
					.encode(new MessageOutput( clntSock.getOutputStream() ));
			} catch (Exception e) { // if failed to encode ERROR
				System.err.print( e.getMessage() );
			}
        }
	}
    
    /**
     * Filters rcvmsg in specified order of consideration
     * 
     * @param received LatitudeMessage to filter
     * 
     * @returns error message (if any)
     * 
     * @throws ValidationException
     *      if validation failure
     */
    private static LatitudeMessage errFilters(LatitudeMessage received) throws ValidationException {
    	// client's LatitudeMessage operation
        String op = received.getOperation();
        switch(op) {
            case LatitudeError.OPERATION:
            case LatitudeLocationResponse.OPERATION:
                return new LatitudeError(received.getMapId(), "Unexpected message type: " + op);
        }
        
        if(received.getVersion() != LatitudeMessage.VERSION) {
            return new LatitudeError(0L, "Unexpected version: " + received.getVersion());
        }

        switch(op) {
            case LatitudeNewLocation.OPERATION:
            case LatitudeLocationRequest.OPERATION: break; // !(NEW || ALL) 
            default: return new LatitudeError(received.getMapId(), "Unknown operation: " + op);
        }
        
        if(received.getMapId() != MAPID) {
            return new LatitudeError(received.getMapId(), "No such map: " + received.getMapId());
        }
        
        if( LatitudeNewLocation.OPERATION.equals(op) ) {
        	// client's userID
            long userId = ((LatitudeNewLocation)received).getLocationRecord().getUserId();
            
            // if user is unregistered
            if(userId2Name.get(userId) == null) {
                return new LatitudeError(received.getMapId(), "No such user ID: " + String.valueOf(userId));
            }
        }
        
        return null; // no errors
    }
    
    /**
     * Load userData from password file into static structures
     * 
     * @param passFile local file of passwords
     */
    private static void load(File passFile) {
        // init maps
        userId2Name = new HashMap<Long, String>();
        userName2Id = new HashMap<String, Long>();
        
        try(var passIn = new Scanner(passFile)) {
            while( passIn.hasNextLine() ) {
            	// user's info loaded into String[]
                String userdata[] = passIn.nextLine().split(":"),
                       userName = userdata[1];
                long userId = Long.parseLong(userdata[0]);
                
                userId2Name.put(userId, userName);
                userName2Id.put(userName, userId);
            }
        } catch (FileNotFoundException e) {
            System.err.print("File not found");
            System.exit(0); // terminate
        }
    }
    
    /**
     * Converts a Location to a LocationRecord
     * 
     * @param location Location to convert
     * 
     * @returns converted LocationRecord
     * 
     * @throws ValidationException
     *      if validation failure
     */
    private LocationRecord convertLoc(Location location) throws ValidationException {
    	return new LocationRecord(getUserId(location), location.getLongitude(), location.getLatitude(),
                location.getName(), location.getDescription());
    }
    
    /**
     * Return userID extracted from Location
     * 
     * @param location map Location with corresponding username
     * 
     * @return user ID
     */
    protected long getUserId(Location location) {
    	// split then immediately dereference for key
    	return userName2Id.get( location.getName().split(":")[0] );
    }
}
