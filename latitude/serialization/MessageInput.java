/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 0
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * Deserialization input source for messages.
 */
public class MessageInput {
    public static final String CHARENC = "ASCII";
    
    private String token;
    private byte[] buf;
    private InputStream in;
    
    /**
     * Constructs a new input source from an InputStream
     * 
     * @param in byte input source
     * 
     * @throws NullPointerException
     *      if in is null
     */
    public MessageInput(InputStream in) throws NullPointerException {
        this.in = Objects.requireNonNull(in, "null InputStream");
        this.buf = new byte[1];
    }
    
    /**
     * Reads the next token from the InputStream
     * 
     * @returns token

     * @throws IOException
     *      if I/O problem
     */
    public String read() throws IOException {
    	// char
        String c = null;
        // token that will be appended char by char
        token = "";

        // read until whitespace
        while(in.read(buf, 0, 1) != -1 && !" ".equals(c = new String(buf, CHARENC))) {
            token += c;
        }
        
        // EOS
        if( token.isEmpty() ) {
            throw new EOFException("Empty token");
        }
        
        return token;
    }
    
    /**
     * Reads the next i bytes from the InputStream
     * 
     * @returns token

     * @throws IOException
     *      if I/O problem
     */
    public String read(int i) throws IOException {
    	// token that will be appended char by char
        token = "";
        // declared outside of loop to catch short packets
        int j;
        
        for(j = 0; j < i && in.read(buf, 0, 1) != -1; j++) {
            token += new String(buf, CHARENC);
        }
        
        if(j < i) {
            throw new IOException("Unable to parse message");
        }
        
        return token;
    }
}
