/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 0
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

/**
 * Serialization output source for messages.
 */
public class MessageOutput {
	private OutputStream out;
    
    /**
     * Constructs a new output source from an OutputStream
     * 
     * @param out byte output source
     * 
     * @throws NullPointerException
     *      if out is null
     */
    public MessageOutput(OutputStream out) throws NullPointerException {
        this.out = Objects.requireNonNull(out, "null OutputStream");
    }
    
    /**
     * Writes message to an OutputStream
     * 
     * @param message String to be converted and written
     * 
     * @returns this for fluid interface
     * 
     * @throws IOException
     *      if I/O problem
     */
    public MessageOutput write(String message) throws IOException {
        byte[] buf = message.getBytes(MessageInput.CHARENC);
        out.write(buf, 0, buf.length);
        
        return this;
    }
}
