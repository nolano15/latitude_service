/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 1
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

import tools.Filters;

/**
 * Represents a generic latitude message
 */
public abstract class LatitudeMessage {
	public static final int VERSION = 1; // stamp with current version
	// specifed header format
	private static final String HEADER = "LATITUDEv",
            				   ENDLINE = "\r\n";
    protected long mapId;
    
    /**
     * Serializes message to MessageOutput
     * 
     * @param out serialization output destination
     * 
     * @throws IOException
     *      if I/O problem
     */
    public final void encode(MessageOutput out) throws IOException {
        if(out == null) {
            throw new NullPointerException("null MessageOutput");
        }
        
        // encode header
        out.write(HEADER + VERSION + " ")
            .write(mapId + " ")
            .write(getOperation() + " ");
        
        // encode specfic message
        encodeSwitch(out);
        
        // encode endline
        out.write(ENDLINE);
    }
    
    /**
     * Encode the specific LatitudeMessage
     * 
     * @param out serialization output destination
     * 
     * @throws IOException
     *      if I/O problem
     */
    protected abstract void encodeSwitch(MessageOutput out) throws IOException;
    
    /**
     * Constructs latitude message using user input
     * 
     * @param in user input source
     * @param out user output (prompt) destination
     * 
     * @returns user customized LatitudeMessage
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public static LatitudeMessage decodeUser(Scanner in, PrintStream out) throws ValidationException {
        out.print("Operation> ");
        // operation of the LatitudeMessage being decoded
        String operation = in.next();
        
        if(!LatitudeLocationRequest.OPERATION.equals(operation) &&
                !LatitudeNewLocation.OPERATION.equals(operation)) {
            throw new ValidationException(operation, "Unknown operation: " + operation); // Invalid Message
        }
        
        out.print("Map ID> ");
        
        // arbitrary init for compiler
        // protecting LatitudeMessage from Scanner's long parsing
        long mapId = 0L;
        try {
            mapId = in.nextLong();
        } catch(Exception e) {
            throw new ValidationException(Long.toString(mapId), "Bad mapID", e);
        }
        
        // decoded LatitudeMessage
        LatitudeMessage latMsg = null;
        
        switch(operation) {
            case LatitudeLocationRequest.OPERATION: latMsg = new LatitudeLocationRequest(mapId);
                break;
            case LatitudeNewLocation.OPERATION:
                latMsg = new LatitudeNewLocation(mapId,
                        new LocationRecord(in, out) // chain streams into LocationRecord's method
                );
            // no default as operation was already validated
        }
        return latMsg;
    }
    
    /**
     * Deserializes message from byte source
     * 
     * @param in deserialization input source
     * 
     * @returns a specific message resulting from deserialization
     * 
     * @throws ValidationException
     *      if validation failure
     * @throws IOException
     *      if I/O problem
     */
    public static LatitudeMessage decode(MessageInput in) throws ValidationException, IOException {
        if(in == null) {
            throw new IOException("null MessageInput");
        }
        
        String header = in.read();
        
        // if latitude version header is wrong
        if( !header.matches(HEADER + "([0-9])") ) {
            throw new ValidationException(header, "bad LatitudeMessage header");
        }

        // return the decoded subclass
        return switchSub(in);
    }
    
    /**
     * Switches the decoding process for each possible operation
     * 
     * @param in deserialization input source
     * 
     * @returns a custom LatitudeMessage resulting from deserialization
     * 
     * @throws ValidationException
     *      if validation failure
     * @throws IOException
     *      if I/O problem
     */
    private static LatitudeMessage switchSub(MessageInput in) throws ValidationException, IOException {
        long mapId = LatitudeProtocol.readId(in);
        // decoded LatitudeMessage
        LatitudeMessage latMsg;
        
        // switch on the operation
        String token = in.read();
        switch(token) {
            case LatitudeNewLocation.OPERATION:  latMsg = new LatitudeNewLocation(mapId, new LocationRecord(in));
                     break;
            case LatitudeLocationRequest.OPERATION:  latMsg = new LatitudeLocationRequest(mapId);
                     break;
            case LatitudeLocationResponse.OPERATION:  latMsg = new LatitudeLocationResponse(mapId, LatitudeProtocol.readLatString(in))
                                .decodeLocs(in); // will decode all mapLocations
                     break;
            case LatitudeError.OPERATION:  latMsg = new LatitudeError(mapId, LatitudeProtocol.readLatString(in));
                     break;
            default: throw new ValidationException(token, "Bad Operation");
        }
        
        // if message has bad endline
        // should be two bytes
        if( !(token = in.read(2)).matches(ENDLINE + "$") ) { // plus anchor
            throw new ValidationException(token, "bad endline");
        }
        
        return latMsg;
    }
    
    /**
     * Returns message operation (e.g., NEW)
     * 
     * @returns message operation
     */
    public abstract String getOperation();

    /**
     * Returns map ID
     * 
     * @returns map ID
     */
    public final long getMapId() {
        return mapId;
    }

    /**
     * Sets map ID
     * 
     * @param mapId  new map ID
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public final void setMapId(long mapId) throws ValidationException {
        try {
        	this.mapId = Filters.filterId(mapId, Filters.UNSIGNED_INT_MAX_POWER);
        } catch(IllegalArgumentException e) {
        	throw new ValidationException(String.valueOf(mapId), "Invalid mapID", e);
        }
    }
    
    /**
     * Returns version
     * 
     * @returns version
     */
    public int getVersion() {
        return VERSION ;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (mapId ^ (mapId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof LatitudeMessage))
            return false;
        LatitudeMessage other = (LatitudeMessage) obj;
        if (mapId != other.mapId)
            return false;
        return true;
    }
}