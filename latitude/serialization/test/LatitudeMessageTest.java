/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 1
* Class: CS 4321
*
************************************************/

package latitude.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import latitude.serialization.*;
import tools.Filters;

/**
 * Class to test the validity of encoding and decoding various messages
 */
class LatitudeMessageTest {
	// Sample LatitudeMessages
    private static final String NEW_ENCODED = "LATITUDEv1 345 NEW 4 5.0 6.0 4 here5 there\r\n",
                                ALL_ENCODED = "LATITUDEv1 345 ALL \r\n",
                                RESPONSE_ENCODED = "LATITUDEv1 345 RESPONSE 6 Campus2 1 1.2 3.4 2 BU6 Baylor4 5.0 6.0 4 here5 there\r\n",
                                ERROR_ENCODED = "LATITUDEv1 345 ERROR 3 bad\r\n";

    /**
     *  Contains all tests related to encoding and decoding LatitudeMessages
     */
    @Nested
    @DisplayName("Encoding/Decoding")
    class EncodeDecodeTests {
    	
        /**
         * Tests creation of NEW LocationRecord via LatitudeMessage
         * 
         * @throws IOException
         *      if I/O problem
         * @throws ValidationException
         *      if validation failure
         */
        @Test
        @DisplayName("Basic New Decode")
        public void testNewDecode() throws IOException, ValidationException {
        	// test general attr
            var msg = LatitudeMessage.decode( getMsgIn(NEW_ENCODED) );
            assertAll(
                    () -> assertEquals(345, msg.getMapId()),
                    () -> assertEquals(LatitudeNewLocation.OPERATION, msg.getOperation())
                    );
            
            // cast then test LatitudeNewLocation attr
            var newMsg = (LatitudeNewLocation) msg;
            assertAll(
                    () -> assertEquals(4, newMsg.getLocationRecord().getUserId()),
                    () -> assertEquals("5.0", newMsg.getLocationRecord().getLongitude()),
                    () -> assertEquals("6.0", newMsg.getLocationRecord().getLatitude()),
                    () -> assertEquals("here", newMsg.getLocationRecord().getLocationName()),
                    () -> assertEquals("there", newMsg.getLocationRecord().getLocationDescription())
                    );
        }
        
        /**
         * Tests creation of LatitudeLocationResponse
         * 
         * @throws ValidationException
         *      if validation failure
         * @throws IOException
         *      if I/O problem
         */
        @Test
        @DisplayName("Basic Response Decode")
        public void testResponseDecode() throws ValidationException, IOException {
        	// test general attr
            var msg = LatitudeMessage.decode( getMsgIn(RESPONSE_ENCODED) );
            assertAll(
                    () -> assertEquals(3, msg.getMapId()),
                    () -> assertEquals(LatitudeLocationResponse.OPERATION, msg.getOperation())
                    );
            
            // cast then test LatitudeLocationResponse attr
            var newMsg = (LatitudeLocationResponse) msg;
            assertAll(
                    () -> assertEquals("Campus", newMsg.getMapName()),
                    () -> assertEquals(Arrays.asList(
                            new LocationRecord(1L, "1.2", "3.4", "BU", "Baylor"),
                            new LocationRecord(4L, "5.0", "6.0", "here", "there")
                            ), newMsg.getLocationRecordList())
                    );
        }
        
        /**
         * Testing that LatitudeMessages can be encoded properly
         * 
         * @param message message to be compared to its recoded version
         * 
         * @throws IOException
         *      if I/O problem
         * @throws ValidationException
         *      if validation failure
         */
        @DisplayName("Encode")
        @ParameterizedTest(name = "message = {0}")
        @ValueSource(strings = {NEW_ENCODED,
                                ALL_ENCODED,
                                RESPONSE_ENCODED,
                                ERROR_ENCODED
                                })
        public void testBasicEncode(String message) throws IOException, ValidationException {
            var bOut = new ByteArrayOutputStream();
            
            // immediately encoding the return should be the same message
            LatitudeMessage
                .decode( getMsgIn(message) )
                .encode(new MessageOutput(bOut));
            
            assertArrayEquals(message.getBytes(MessageInput.CHARENC), bOut.toByteArray());
        }

        /**
         * Testing various invalid LatitudeMessages
         * 
         * @param message broken message being tested
         */
        @DisplayName("Invalid Decoding")
        @ParameterizedTest(name = "message = {0}")
        @ValueSource(strings = {
                "LATITUDE1 345 NEW 4 5.0 6.0 4 here5 there\r\n",
                "LATITUDEv1 -345 NEW 4 5.0 6.0 4 here5 there\r\n",
                "LATITUDEv1 ? NEW 4 5.0 6.0 4 here5 there\r\n",
                "LATITUDEv1 NEW 4 5.0 6.0 4 here5 there\r\n",
                "LATITUDEv1 345 ?? 4 5.0 6.0 4 here5 there\r\n",
                })
        public void testInvalidDecode(String message) {
            assertThrows(ValidationException.class, () -> LatitudeMessage.decode( getMsgIn(message) ));
        }
    }
    
    /**
     * Testing MapID specifications
     */
    @Nested
    @DisplayName("MapID")
    class UserIDTests {
        /**
         * Testing mapID setter and getter
         * 
         * @throws ValidationException
         *      if validation fails
         * @throws IOException
         *      if I/O problem
         */
        @Test
        @DisplayName("Basic")
        public void testBasic() throws ValidationException, IOException {
        	// decode vanilla LatitudeMessage
            LatitudeMessage test = LatitudeMessage.decode( getMsgIn(ALL_ENCODED) );
            
            test.setMapId((long)Math.pow(2, Filters.UNSIGNED_INT_MAX_POWER) - 1);
            assertEquals(test.getMapId(), Math.pow(2, Filters.UNSIGNED_INT_MAX_POWER) - 1);
        }
            
        /**
         * Testing max mapID
         */
        @Test
        @DisplayName("Invalid")
        public void testInvalid() {
            assertAll(
                () -> assertThrows(ValidationException.class, () -> LatitudeMessage
                        .decode(getMsgIn(ALL_ENCODED))
                        .setMapId( (long)Math.pow(2, Filters.UNSIGNED_INT_MAX_POWER) ), "large mapID"),
                () -> assertThrows(ValidationException.class, () -> LatitudeMessage
                        .decode(getMsgIn(ALL_ENCODED))
                        .setMapId(-1L), "negative mapID")
                );
        }
    }
    
    /**
     * Testing that IOException is thrown
     */
    @DisplayName("IO Test")
    @ParameterizedTest(name = "message = {0}")
    @ValueSource(strings = {
            "LATITUDEv1 345 ERROR 4 bad\r\n",
            "LATITUDEv1 345 RESPONSE 6 Campus3 1 1.2 3.4 2 BU6 Baylor4 5.0 6.0 4 here5 there\r\n"
            })
    public void testIO(String message) {
        assertThrows(IOException.class, () -> LatitudeMessage.decode( getMsgIn(message) ));
    }
    
    /**
     * Factory method with custom message
     * 
     * @param b string to be converted to bytes
     * 
     * @return vanilla MessageInput
     * 
     * @throws IOException
     *      if I/O problem
     * @throws NullPointerException
     *      if b is null
     */
    public static MessageInput getMsgIn(String b) throws IOException {
        return new MessageInput(new ByteArrayInputStream( b.getBytes(MessageInput.CHARENC) ));
    }
}
