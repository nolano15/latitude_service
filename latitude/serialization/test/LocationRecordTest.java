/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 0
* Class: CS 4321
*
************************************************/

package latitude.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import latitude.serialization.*;

/**
 * Tests for the LocationRecord class
 */
class LocationRecordTest {
	// Sample encoded LocationRecord
    private static final String DEFAULT_ENCODED = "1 1.2 3.4 2 BU6 Baylor";
    
    /**
     * Returns a vanilla LocationRecord
     * 
     * @returns new default LocationRecord
     * 
     * @throws ValidationException
     *      if validation fails
     */
    private LocationRecord getDefault() throws ValidationException {
        return new LocationRecord(1L, "1.2", "3.4", "BU", "Baylor");
    }
    
    /**
     *  Contains all tests related to encoding and decoding LocationRecords
     */
    @Nested
    @DisplayName("Encoding/Decoding")
    class EncodeDecodeTests {
        /**
         * Testing a typical encode
         * 
         * @throws IOException
         *      if MessageOutput has an error
         * @throws ValidationException
         *      if validation fails
         */
        @Test
        @DisplayName("Basic Encode")
        public void testEncode() throws IOException, ValidationException {
            var bOut = new ByteArrayOutputStream();
            getDefault().encode(new MessageOutput(bOut));
            assertArrayEquals(DEFAULT_ENCODED.getBytes(MessageInput.CHARENC), bOut.toByteArray());
        }
        
        /**
         *  Contains only decoding tests
         */
        @Nested
        @DisplayName("Decoding")
        class DecodeTests {
            /**
             * Testing a typical decode
             * 
             * @throws IOException
             *      if MessageInput has an error
             * @throws ValidationException
             *      if validation fails
             */
            @Test
            @DisplayName("Basic Decode")
            public void testBasicDecode() throws IOException, ValidationException {
            	// decoded LocationRecord
                var test = new LocationRecord( getMsgIn(DEFAULT_ENCODED) );
                
                assertAll(
                        () -> assertEquals(1L, test.getUserId()),
                        () -> assertEquals("1.2", test.getLongitude()),
                        () -> assertEquals("3.4", test.getLatitude()),
                        () -> assertEquals("BU", test.getLocationName()),
                        () -> assertEquals("Baylor", test.getLocationDescription())
                        );
            }
            
            /**
             * Testing that invalid messages are not allowed
             * 
             * @throws IOException
             *      if MessageInput has an error
             */
            @DisplayName("Invalid Decoding")
            @ParameterizedTest(name = "message = {0}")
            @ValueSource(strings = {"-1 1.2 3.4 2 BU6 Baylor",
                                    "? 1.2 3.4 2 BU6 Baylor",
                                    "1 181.0 3.4 2 BU6 Baylor",
                                    "1 -181.0 3.4 2 BU6 Baylor",
                                    "1 1.2 91.0 2 BU6 Baylor",
                                    "1 1.2 -91.0 2 BU6 Baylor",
                                    "1 1.2 3.4 ? BU6 Baylor",
                                    "1 0 3.4 2 BU6 Baylor",
                                    "1 .0 3.4 2 BU6 Baylor",
                                    "4294967296 1.2 3.4 2 BU6 Baylor"
                                    })
            public void testInvalidDecode(String message) throws IOException {
                assertThrows(ValidationException.class, () -> new LocationRecord( getMsgIn(message) ));
            }
            
            /**
             * Factory method with custom message
             * 
             * @param b string to be converted to bytes
             * 
             * @return vanilla MessageInput
             * 
             * @throws IOException
             *      if I/O problem
             * @throws NullPointerException
             *      if b is null
             */
            public MessageInput getMsgIn(String b) throws IOException {
                return new MessageInput(new ByteArrayInputStream( b.getBytes(MessageInput.CHARENC) ));
            }
        }
    }
}
