/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;

import tools.BinaryTools;
import tools.Filters;

/**
 * Superclass for Register/Deregister NoTiFiMessages
 */
public abstract class NoTiFiRegisterOption extends NoTiFiMessage {
	// address + port will be allocated 6 bytes
	protected static final int BYTES = 6,
							   IP_BYTES = 4;
	protected Inet4Address address;
	protected int port;
	
	/**
	 * Constructs NoTiFi register/deregister from ByteBuffer
	 * 
	 * @param msgId message ID
	 * @param bb ByteBuffer packet
	 * 
	 * @throws IllegalArgumentException
	 * 		if message ID invalid
	 * @throws IOException
	 * 		if I/O problem including packet too long/short (EOFException)
	 */
	public NoTiFiRegisterOption(int msgId, ByteBuffer bb) throws IllegalArgumentException, IOException {
		this(msgId, decodeIP( BinaryTools.getBuffBytes(bb, IP_BYTES) ),
				bb.getShort()&0xffff); // promote to int, then AND to keep short bits
		
		if(bb.limit() != getBytes()) {
			throw new IOException("Short packet");
		}
	}
	
	/**
	 * Constructs NoTiFi register/deregister from values
	 * 
	 * @param msgId message ID
	 * @param address address to register
	 * @param port port to register
	 * 
	 * @throws IllegalArgumentException
	 * 		if message ID invalid
	 */
	public NoTiFiRegisterOption(int msgId, Inet4Address address, int port) throws IllegalArgumentException {
		this.setMsgId(msgId);
		this.setAddress(address);
		this.setPort(port);
	}
	
	/**
	 * Serializes NoTiFiRegister/Deregister
	 * 
	 * @param code specific NoTiFiMessage code
	 * 
	 * @return serialized message bytes
	 */
	protected byte[] encodeReg(int code) {
		return encodeHeader(getBytes(), code)
				.put( BinaryTools.reverse( address.getAddress() ) ) // add IP address
				.order(ByteOrder.LITTLE_ENDIAN)
				// port is specified two bytes, so cast to short
				.putShort((short)port) // add port
				.array();
	}
	
	/**
	 * Decodes IP address from byte array
	 * 
	 * @param b byte array to convert to IP address
	 * 
	 * @return IP address
	 * 
	 * @throws IOException
	 * 		if host cannot be resolved
	 */
	public static Inet4Address decodeIP(byte[] b) throws IOException {
		// reverse into network order
		return (Inet4Address)InetAddress.getByAddress( BinaryTools.reverse(b) );
	}
	
	@Override
	public int getBytes() {
		return HEADER_SIZE + BYTES;
	}
	
	/**
	 * Get address
	 * 
	 * @return address
	 */
	public Inet4Address getAddress() {
		return address;
	}

	/**
	 * Set address
	 * 
	 * @param address address
	 * 
	 * @throws IllegalArgumentException
	 * 		if address is null
	 */
	public void setAddress(Inet4Address address) throws IllegalArgumentException {
		try {
			this.address = Objects.requireNonNull(address);
		} catch(NullPointerException e) {
			throw new IllegalArgumentException("null address", e);
		}
	}

	/**
	 * Get port
	 * 
	 * @return port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Set port
	 * 
	 * @param port port
	 * 
	 * @throws IllegalArgumentException
	 * 		if port is out of range [0...65535]
	 */
	public void setPort(int port) throws IllegalArgumentException {
		this.port = (int)Filters.filterId(port, Filters.PORT_MAX_POWER);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + port;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoTiFiRegisterOption other = (NoTiFiRegisterOption) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (port != other.port)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NoTiFiRegisterOption [address=" + address + ", port=" + port + "]";
	}
}
