/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

/**
 * NoTiFi location addition
 */
public class NoTiFiLocationAddition extends NoTiFiLocationOption {
	// Code for NoTiFi location addition
	public static final int CODE = 1;
	
	/**
	 * Constructs NoTiFi location addition from values
	 * 
	 * @param msgId message ID
	 * @param location location to add
	 * 
	 * @throws IllegalArgumentException
	 * 		if message ID invalid
	 */
	public NoTiFiLocationAddition(int msgId, NoTiFiLocation location) throws IllegalArgumentException {
		this.setMsgId(msgId);
		this.setLocation(location);
	}
	
	@Override
	public int getCode() {
		return CODE;
	}

	@Override
	public byte[] encode() {
		return encodeOpt(CODE);
	}
	
	@Override
    public String toString() {
        return "Addition\n" + location;
    }
}
