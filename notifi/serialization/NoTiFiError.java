/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import tools.BinaryTools;
import tools.Filters;

/**
 * NoTiFi error
 */
public class NoTiFiError extends NoTiFiMessage {
	// Code for NoTiFi error
	public static final int CODE = 4;
	private String errorMessage;
	
	/**
	 * Constructs NoTiFi error from ByteBuffer
	 * 
	 * @param msgId message ID
	 * @param bb ByteBuffer packet
	 * 
	 * @throws IllegalArgumentException
	 * 		if message ID invalid
	 * @throws IOException
	 * 		if I/O problem including packet too long/short (EOFException)
	 */
	public NoTiFiError(int msgId, ByteBuffer bb) throws IllegalArgumentException, IOException {
		this(msgId,
				// read rest of packet
				new String(BinaryTools.getBuffBytes(bb, bb.remaining()), NoTiFiLocation.CHARENC));
	}
	
	/**
	 * Constructs NoTiFi error from values
	 * 
	 * @param msgId message ID
	 * @param errorMessage error message
	 * 
	 * @throws IllegalArgumentException
	 * 		if values invalid
	 */
	public NoTiFiError(int msgId, String errorMessage) throws IllegalArgumentException {
		this.setMsgId(msgId);
		this.setErrorMessage(errorMessage);
	}
	
	@Override
	public byte[] encode() {
		try {
			return encodeHeader(HEADER_SIZE + errorMessage.length(), CODE)
					.put( errorMessage.getBytes(NoTiFiLocation.CHARENC) ) // add error msg
					.array();
		} catch (UnsupportedEncodingException e) {
			// ASCII will be supported
		}
		return null; // compiler
	}
	
	@Override
	public int getCode() {
		return CODE;
	}
	
	@Override
	public int getBytes() {
		return HEADER_SIZE + errorMessage.length();
	}

	/**
	 * Get error message
	 * 
	 * @return error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Set error message
	 * 
	 * @param errorMessage error message
	 * 
	 * @throws IllegalArgumentException
	 * 		if error message is null or non-ASCII
	 */
	public void setErrorMessage(String errorMessage) throws IllegalArgumentException {
		this.errorMessage = Filters.filterASCII(errorMessage);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((errorMessage == null) ? 0 : errorMessage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoTiFiError other = (NoTiFiError) obj;
		if (errorMessage == null) {
			if (other.errorMessage != null)
				return false;
		} else if (!errorMessage.equals(other.errorMessage))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return errorMessage;
    }
}
