/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

import java.io.IOException;
import java.net.Inet4Address;
import java.nio.ByteBuffer;

/**
 * NoTiFi deregister
 */
public class NoTiFiDeregister extends NoTiFiRegisterOption {
	// Code for NoTiFi deregister
	public static final int CODE = 3;
	
	public NoTiFiDeregister(int msgId, ByteBuffer bb) throws IllegalArgumentException, IOException {
		super(msgId, bb);
	}

	public NoTiFiDeregister(int msgId, Inet4Address address, int port) throws IllegalArgumentException {
		super(msgId, address, port);
	}
	
	@Override
	public byte[] encode() {
		return encodeReg(CODE);
	}
	
	@Override
	public int getCode() {
		return CODE;
	}
}
