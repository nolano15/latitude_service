/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiLocationAddition;
import notifi.serialization.NoTiFiMessage;

/**
 * NoTiFi location addition test
 */
class NoTiFiLocationAdditionTest {
	// Sample NoTiFi Location Addition packet
	private static final byte[] LOCADD_PKT = new byte[] {0x31, 0x1,
			0x0, 0x0, 0x0, 0x6, // userID
			0x33, 0x33, 0x33, 0x33,
			0x33, 0x33, (byte)0xf3, 0x3f, // longitude
			0x33, 0x33, 0x33, 0x33,
			0x33, 0x33, 0xb, 0x40, // latitude
			0x2, 'B', 'U',
			0x6, 'B', 'a', 'y', 'l', 'o', 'r'
			};
	//0x3ff3...
	
	/**
     *  Contains all basic encoding/decoding tests
     */
    @Nested
    @DisplayName("Basic")
    class BasicTests {
    	/**
    	 * Testing a typical decode
    	 * 
    	 * @throws IllegalArgumentException
    	 * 		if values invalid
    	 * @throws IOException
    	 * 		if I/O problem including packet too long/short (EOFException)
    	 */
    	@Test
    	@DisplayName("Decode")
    	void testDecode() throws IllegalArgumentException, IOException {
    		// test general attr
    		var msg = NoTiFiMessage.decode(LOCADD_PKT);	
    		assertAll(
    			() -> assertEquals(1, msg.getMsgId()),
                () -> assertEquals(NoTiFiLocationAddition.CODE, msg.getCode())
            );
    		
    		// cast then test NoTiFiLocationAddition attr
    		var newLoc = ((NoTiFiLocationAddition)msg).getLocation();
    		
    		assertAll(
    			() -> assertEquals(6, newLoc.getUserId()),
    			() -> assertEquals(1.2, newLoc.getLongitude()),
    			() -> assertEquals(3.4, newLoc.getLatitude()),
    			() -> assertEquals("BU", newLoc.getLocationName()),
    			() -> assertEquals("Baylor", newLoc.getLocationDescription())
            );
    	}
    	
    	/**
    	 * Testing a typical encode
    	 * 
    	 * @throws IllegalArgumentException
    	 * 		if values invalid
    	 * @throws IOException
    	 * 		if I/O problem including packet too long/short (EOFException)
    	 */
    	@Test
    	@DisplayName("Encode")
    	void testEncode() throws IllegalArgumentException, IOException {
            // immediately encoding the return should be the original packet
            assertArrayEquals(LOCADD_PKT, NoTiFiMessage.decode(LOCADD_PKT).encode());
    	}
    }
    
    /**
     * Contains all invalid tests
     */
    @Nested
    @DisplayName("Invalid Tests")
    class InvalidTests {
    	// Test packets with bad versions/codes
    	@Test
    	@DisplayName("Decoding")
        void testInvalidDecode() {
    		assertThrows(IOException.class,
    				() -> NoTiFiMessage.decode(new byte[] {0x31, 0x5,
					0x1, 0x0, 0x0, 0x0, // userID
					0x0, 0x0, 0x0, 0x0,
					0x3f, (byte)0x99, (byte)0x99, (byte)0x9a, // longitude
					0x0, 0x0, 0x0, 0x0,
					0x40, 0x59, (byte)0x99, (byte)0x9a, // latitude
					0x2, 'B', 'U',
					0x6, 'B', 'a', 'y', 'l', 'o',
					}), "Short packet");
        }

    	// Test null and out-of-bounds args
    	@Test
    	@DisplayName("Field tests")
        void testInvalidFields() {
    		assertThrows(IllegalArgumentException.class,
    				() -> new NoTiFiLocationAddition(5, null), "Null location");
        }
    }
}
